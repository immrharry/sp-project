
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <error.h>
#include <errno.h>
#include <signal.h>

char buff[1024];
char buff2[1024];
char buff3[1024];
char msg1[100];
int rval2;
int sock;

void signal_handler(int signo) {
	if(signo == SIGINT) {
		write(STDOUT_FILENO, "Interrupt signal received.", 27);
		exit(0);
	}

	if(signo == SIGTERM) {
		write(STDOUT_FILENO, "Terminate signal received.", 26);
		exit(0);
	}
	

}


//Thread func
void *some() {

	while(1) {
		rval2 = read(sock, buff2, 1024);
		write(STDOUT_FILENO, buff2, rval2);
	}
}



int main(int argc, char *argv[]) {
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	struct sockaddr_in server;
	struct hostent *hp;
	char buf[1024];
	
	
	

	void *some();

    	pthread_t thread1;
    	int iret1;

    	iret1=pthread_create(&thread1, NULL, some, NULL);
    	if(iret1) {
        	perror("Thread failed to execute");
        	exit(EXIT_FAILURE);
   	}

	

	/* Create socket */
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		perror("opening stream socket");
		exit(1);
	}
	
	while(1) {
		
	
		/* Connect socket using name specified by command line. */
		server.sin_family = AF_INET;
		hp = gethostbyname(argv[1]);
		if (hp == 0) {
		perror("");
		fprintf(stderr, "%s: unknown host\n", argv[1]);
		exit(2);
		}
		bcopy(hp->h_addr, &server.sin_addr, hp->h_length);
		//inet_aton(ip, &server.sin_addr.s_addr);
		server.sin_port = htons(atoi(argv[2]));

		if (connect(sock,(struct sockaddr *) &server,sizeof(server)) < 0) {
		perror("connecting stream socket");
		exit(1);
		}
	
		while(1) {
			//char buff[50];
			int rc = read(STDIN_FILENO, buff, 50); 

			if(rc == 0) {
            			write(STDOUT_FILENO, "Invalid Command.\n", 16);
     	 		}
			//int writesck = write(sock, buff, rc);
			buff[rc - 1] = '\0';
			if((strcasecmp(buff, "exit") == 0)) {
        			write(STDOUT_FILENO, "Client Disconnected.\n", 22);
        			close(sock);
        			exit(0);
			}
			else {
				if (write(sock, buff, (rc - 1))< 0) {
					perror("writing on stream socket");
				}	
	
			}
			
			
		}
		
		close(sock);
	}
	
}


