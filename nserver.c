#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/wait.h>
#include <wait.h>
#include <signal.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <error.h>
#include <errno.h>
#include <pthread.h>

#define TRUE 1

char *array;
char ipaddr[50];
int msgsock;
int valread;
char msg[100];

struct Process {
	char name[50];
	int pid;
 	char status[50];
	time_t stime;
	time_t etime;
	char clientipadd[50];
	
};


void *serThread() {
	int ser = write(1, "Do you have message?", sizeof("Do you have message?"));
	while(1) {
		int readc = read(0, msg, sizeof(msg));
		
		write(msgsock, msg, readc);
	}
}

struct Process *pro;

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;
int pc = 0;
int xc = 0;

void signal_handler(int signo) {
	if(signo == SIGINT) {
		write(STDOUT_FILENO, "\nInterrupt signal received.\n", 29);
		exit(0);
	}

	if(signo == SIGTERM) {
		write(STDOUT_FILENO, "\nTerminate signal received.\n", 28);
		exit(0);
	}

	if(signo == SIGCHLD) {
		write(STDOUT_FILENO, "\nsignal received.\n", 28);
	}

}



struct myClients {
	char ip[50];
	char cstat[20];
};
struct myClients *cli;


void run(char *array) {

	write(STDOUT_FILENO, "Running process\n", 16);
	int status;
	char path[50];
    	strcpy(path,"/usr/bin/");

	int cpid = fork();

	if (cpid==0) {
		strcat(path, array);
		int a = execl(path,"",NULL);
        	if(a==-1) {
            		write(msgsock, "No such file\n", 13);
            		perror("exec failed");
            		exit(EXIT_FAILURE);
        	}
	}

	else if	(cpid>0) {
		
		pid_t id = waitpid(cpid, &status, WNOHANG);
		if (id == 0) {
			  write(STDOUT_FILENO, "Exec successful\n", 16);
			  pthread_mutex_lock(&mutex1);
            		  pro[pc].pid = cpid;
           		  

            		  strcpy(pro[pc].name, array);
            		
            		  strcpy(pro[pc].status, "active");

			  struct timeval tv1;
			  gettimeofday(&tv1, NULL);
			  pro[pc].stime = tv1.tv_sec;
			  pro[pc].etime = 0;
           		  
			  strcpy(pro[pc].clientipadd, ipaddr);
			  pc++;
			  pthread_mutex_unlock( &mutex1 );
			 
            		  write(STDOUT_FILENO, " \n", 2);
		}

		else {
			write(STDOUT_FILENO, "Exec unsuccessful\n", 18);
		}
	}

	else if (cpid == -1) {
		perror("Fork failed");
	}

}


void add(char *arr) { 

	int sum = 0;
	while(arr != NULL) { 
		int a = atoi(arr); 
		sum += a;
		arr = strtok(NULL, " \n");
		
	}
	char w[50];
	int count = sprintf(w, "Result: %d\n", sum); 
	write(msgsock, w, count);
	
}


void subtract(char *arr) { 
	int subt = (int) atoi(arr); 

	arr = strtok(NULL, " \n"); 
        while (arr != NULL) {
	    
            int a = atoi(arr);
            subt = subt - a;
            arr = strtok(NULL, " \n");
        }

	char w[50];
	int count=sprintf(w, "Result: %d\n", subt);
	write(msgsock, w, count);
	
}

void multiply(char *arr) { //To multiply numbers --> args > 1

    int mult = 1;
    
    while (arr != NULL) {
	
        int a = atoi(arr);
        mult = mult * a;
        arr = strtok(NULL, " \n");
    }
    char w[50];
    int count=sprintf(w, "Result: %d\n", mult);
    write(msgsock, w, count);
    
}


void list() {




write(msgsock, "P_NAME		P_ID		P_STATUS		PSTART_TIME		PEND_TIME		PCLIENT_ADDR", sizeof("P_NAME		P_ID		P_STATUS		PSTART_TIME		PEND_TIME		PCLIENT_ADDR"));
write(msgsock, " \n", 2);

int j;
for (j = 0; j < pc; j++) {
	
    	
        	char temp[500];
        	char list = sprintf(temp, "\n%s		%d		%s		%ld		%ld		%s", pro[j].name, pro[j].pid, pro[j].status, pro[j].stime, pro[j].etime, pro[j].clientipadd);

    	write(msgsock, temp, list);
    	write(msgsock, " \n", 2);

    	
}



}



void terminate() {

int freshkill = 0;

int count = 0;
int j;
for(j = 0; j < pc; j++) {
	
	if((strcasecmp(pro[j].name, array) == 0) && (strcasecmp(pro[j].status, "active") == 0) && (strcasecmp(pro[j].clientipadd, ipaddr) == 0)) {
		freshkill=kill(pro[j].pid, SIGTERM);
                if(freshkill == 0) {
			strcpy(pro[j].status, "inactive");
			struct timeval tv2;
			gettimeofday(&tv2, NULL);
			pro[j].etime = tv2.tv_sec;
                        write(msgsock, "Program terminated.\n", 20);
                }
                else if (freshkill == -1) {
                    perror("Can't kill\n");
                }
                count++;
        }
        else if(pro[j].pid == atoi(array)  && (strcasecmp(pro[j].status, "active") == 0) && (strcasecmp(pro[j].clientipadd, ipaddr) == 0)) {
		freshkill = kill(pro[j].pid, SIGTERM);
                if(freshkill == 0) {
                    strcpy(pro[j].status,"inactive");
		    struct timeval tv2;
		    gettimeofday(&tv2, NULL);
		    pro[j].etime = tv2.tv_sec;
                    write(msgsock, "Program terminated.\n", 20);
                }
                else if (freshkill == -1) {
                    perror("Can't kill\n");
                }
                count++;
        }

}
	if(count == 0) {
		
		write(msgsock, "Unable to terminate. Program name or id mismatch or no program name given.\n", 65);
	}



else {
     write(msgsock,"Invalid run command\n",20);
}


}

void listmeClients() {
	write(msgsock, "CLIENT IP		STATUS", sizeof("CLIENT IP		STATUS"));
write(msgsock, " \n", 2);
	for(int i = 0; i < xc; i++) {
		char temp1[500];
		
		char list1 = sprintf(temp1, "\n%s		%s", cli[i].ip, cli[i].cstat);

    	write(msgsock, temp1, list1);
    	write(msgsock, " \n", 2);

	}
}

/*void sendMsg(char *msg, char) {

}*/


int main() {

signal(SIGTERM, signal_handler);
signal(SIGINT, signal_handler);

pro = (struct Process*)malloc(99*sizeof(struct Process));
cli = (struct myClients*) malloc(99 * sizeof(struct myClients));
	
	void *serThread();
	pthread_t threadser;
	int s1 = pthread_create(&threadser, NULL, serThread, NULL);

	int sock, length;
	struct sockaddr_in server;
	struct sockaddr_in client;
	char buf[1024];
	char buf2[1024];
	int rval;
	int rval2;
	int i;

int clientlength = sizeof(client);

	/* Create socket */
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		perror("opening stream socket");
		exit(1);
	}
	/* Name socket using wildcards */
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = 0;
	if (bind(sock, (struct sockaddr *) &server, sizeof(server))) {
		perror("binding stream socket");
		exit(1);
	}
	/* Find out assigned port number and print it out */
	length = sizeof(server);
	if (getsockname(sock, (struct sockaddr *) &server, &length)) {
		perror("getting socket name");
		exit(1);
	}
	printf("Socket has port #%d\n", ntohs(server.sin_port));
	fflush(stdout);

	/* Start accepting connections */
	listen(sock, 5);
	
	do {

        			msgsock = accept(sock, (struct sockaddr *) &client, &clientlength);
				
				if(msgsock == -1)
					perror("connection");
				
				else {
					int c_id = fork();
					if(c_id == -1){
						perror("client fork");
					}
					if(c_id == 0) {
						
					 	inet_ntop(AF_INET, &(client.sin_addr), ipaddr, 50);
						pthread_mutex_lock(&mutex2);
						strcpy(cli[xc].ip, ipaddr);
            		  			strcpy(cli[xc].cstat, "active");
						char yt[100];
						int gh = sprintf(yt, "Connection established from client with IP: %s\n", cli[xc].ip);
						write(STDOUT_FILENO, yt, gh);
					
						

					 do {
					   
					   bzero(buf, sizeof(buf));
					   if ((rval = read(msgsock, buf, 1024)) < 0)
						perror("reading stream message");
					   i = 0;
					  
					   if (rval == 0) {
						char fg[100];
						int vb = sprintf(fg, "Ending connection from client with IP: %s\n", cli[xc].ip);
						write(STDOUT_FILENO, fg, vb);
						strcpy(cli[xc].cstat, "inactive");

					   }
					   else {
						
							
							array = strtok(buf, " ");

							if (array==NULL)
                        				continue;

							if(strcasecmp(array, "run")==0) {
                            
                                			array =strtok(NULL,"\n");
							if(array == NULL) 
							continue;
							else
                                			run(array);
                                			}

							else if(strcasecmp(array, "add")==0) {
							array = strtok(NULL, " \n");
							add(array);
							}

							else if(strcasecmp(array, "subt")==0) {
							array = strtok(NULL, " \n");
							subtract(array);
							}

							else if (strcasecmp(array, "mult")==0) {
                                			array =strtok(NULL, " \n");
                               				multiply(array);
                                			}

							else if(strcasecmp(array, "list")==0) {
							array = strtok(NULL, " \n");
							list();
							}

							else if(strcasecmp(array, "terminate")==0) {
							
							array = strtok(NULL, "\n");
							if(array == NULL) 
							continue;
							else
							terminate();
							}

						
							else {
								write(msgsock, "Invalid command\n", 16);
							}

							int chk = write(msgsock,"Acknowledged.\n",14);
							if(chk == -1){
							perror("write");
							}
							write(STDOUT_FILENO, " \n", 2);
							
					   }
					} while(rval != 0);
					close(msgsock);
				}
				if(c_id > 0) {
					xc++;
					pthread_mutex_unlock(&mutex2);			
				}
			}
		} while(1);
}

